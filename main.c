#include <stdio.h>
#include <string.h>

unsigned char ConvertHexChar(char ch)
{
	if((ch>='0')&&(ch<='9'))   
		return   ch-0x30;   
	else   if((ch>='A')&&(ch<='F'))   
		return   ch-'A'+10;   
	else   if((ch>='a')&&(ch<='f'))   
		return   ch-'a'+10;   
	else   return   (-1);  
}
unsigned char ConvertDecChar(char ch)
{
	if((ch>='0')&&(ch<='9'))   
		return   ch-0x30;  
	else   return   (-1);  
}
int String2Num(char *str, unsigned int *data)
{
	unsigned char   hexdata, decdata;
	int   hexdatalen=0;
	int   decdatalen=0;
	int   len=sizeof(str);
	int   i=0;
	if( (str[0]=='0'&&str[1]=='x') || (str[0]=='0'&&str[1]=='X') )
	{
		for(i=2;i<len;i++)
		{
			char   hstr=str[i];
			hexdata=ConvertHexChar(hstr);		
			if(hexdata>=16)
				return -1;
			else
				*data = *data*16 + hexdata;
			hexdatalen++;
		}
		return hexdatalen;
	}
	else
	{
		for(i=0;i<len;i++)
		{
			char   dstr=str[i];
			decdata=ConvertDecChar(dstr);
			if(decdata>=10)
				return -1;
			else
				*data = *data*10 + decdata;
			decdatalen++;
		}
		return decdatalen;
	}
}

int main(int argc, char* argv[])
{
	if(argc == 1){
		printf("missing operand!");
		return -1;
	}
	char* inputfilename = argv[1];	
	FILE *fp_i = NULL;
	char* outputfilename;
	FILE *fp_o = NULL;
	unsigned int data = 0;	
	char line[1024];
	char *save_ptr;
	char x;
	
	fp_i = fopen(inputfilename, "r+");
	if(fp_i == NULL) {
		printf("open csv error!");
		fclose(fp_i);
		return -1;
	}
	if(argc == 2)
	{
		fp_o = fopen("Params.bin", "w+");
	}
	else
	{
		outputfilename = argv[2];
		fp_o = fopen(outputfilename, "w+");
	}
	if(fp_o == NULL) {
		fclose(fp_i);
		fclose(fp_o);
		return -1;
	}
	
	while(fgets(line, 1024, fp_i)) {		
		char *index = strtok_r(line, ",", &save_ptr);
		if (index == NULL) {
			fclose(fp_i);
			fclose(fp_o);
			return -1;
		}  
		char *param = strtok_r(NULL, ",", &save_ptr);
		data = 0;
		if(String2Num(param, &data))
		{
			fputc(data&0xff, fp_o);
			fputc((data>>8)&0xff, fp_o);
		}
		else
			continue;
		char *name = strtok_r(NULL, ",", &save_ptr);
		//printf("%s\t%d\t%s\t\n", index, data, name);
	}
	fclose(fp_i);
	fclose(fp_o);
	return 0;
}
